import 'package:flutter/material.dart';
import 'package:flutter_app/models/Photo.dart';

class DetailPage extends StatelessWidget {
  final Photo item;

  // receive data from the list screen as a parameter
  DetailPage({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Details')),
      body: Container(
        child: Column(
          children: <Widget>[
            Text(
              item.title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24),
            ),
            Image.network(
              item.url,
              fit: BoxFit.fitHeight,
            )
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        padding: EdgeInsets.all(10.0),
      ),
    );
  }
}
